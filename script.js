'use strict'


// Функции нужны для копирования и правильного хранения больших часто используемых частей кода 
// да и вообще для того что б код легче читался и не появлялось лишних повторений частей кода
//
//
// так как параметр это независимая пременная, то сама функция зависит от него
// а Аргумент это передавемые значения которые копируются в параметры функции
// следовательно аргументы нужны для предачи (и получения) значений в параметры функции
// а так же для избежания лишнего использования внешних переменных
// тем более функция которая изменяет внешние переменные может повлечь за собой некоторые последствия
function validateNumber(number) {
    if(isNaN(number)) {
        return false
    } 
    return true
}
function validateOperator(oper) {
    if((oper != "+" && oper != "-" && oper !=  "*" && oper != "/" && oper != "^")) {
        return false
    } 
    return true
}
const getData = (question, defaultValue, validationFunc) => {
    defaultValue = prompt(question)   
    while(!validationFunc(defaultValue)){
        defaultValue = prompt(`${question} ещё раз`)
    }
    console.log(defaultValue) //проверка вывода
    return defaultValue
}
const runCalculator = (a, b, c) => {
    switch (b) {
         case "+":
             return Number(a) + Number(c)
          case "-":
             return a - c
          case "*":
             return a * c
          case "/":
             return a / c
          case "^":
             return a ** c
         default:
          break;
    }
}
console.log(runCalculator(getData("введите ПЕРВЫЙ операнд", 0, validateNumber), getData("введите оператор", 0, validateOperator), getData("введите ВТОРОЙ операнд", 0, validateNumber)))